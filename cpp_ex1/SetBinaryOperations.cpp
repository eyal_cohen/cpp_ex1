//
//  SetBinaryOperations.cpp
//

#include <iostream>
#include <string>
#include "MySet.h"


int main()
{
    MySet set1, set2, set3;
    
    set1.add("a", 1.5);
    set1.add("b", 1.5);
    set1.add("c", 1.5);
    set1.add("d", 1.5);
    set1.add("e", 1.5);

    set2.add("A", 0.5);
    set2.add("B", 0.5);
    set2.add("C", 0.5);
    set2.add("D", 0.5);
    set2.add("E", 0.5);

    set3.add("a", 0.5);
    set3.add("b", 0.5);
    set3.add("c", 0.5);
    set3.add("D", 0.5);
    set3.add("E", 0.5);
    
    // Check operator-
    MySet set4 = (set1 - set2);
    if(set1.size() != set3.size())
    {
        std::cout << "Error: set3 and set1 should be of the same size" << std::endl;
    }
    
    // Check operator&
    MySet set5 = (set1 & set2);
    if(set5.size() != 0)
    {
        std::cout << "Error: set1 and set2 should not intersect" << std::endl;
    }
    
    MySet set6 = (set1 & set3);
    if(set6.size() != 3)
    {
        std::cout << "Error: set1 and set3 should have 3 nodes in common" << std::endl;
    }
    
    // Check operator|
    MySet set7 = (set1 | set2);
    if(set7.size() != set1.size() + set2.size())
    {
        std::cout << "Error: Union of set1 and set2 should be of size 12" << std::endl;
    }
    
    // Check operator=
    MySet set8;
    set8 = set1 = set2;
    if(set8.size() != set2.size())
    {
        std::cout << "Error: Asignment operator failed. set8 should be copy of set2." << std::endl;
    }
    
    std::cout << "End of tests." << std::endl;
    return 0;
}