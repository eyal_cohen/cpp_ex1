//
//  MySet.cpp
//

#include <iostream>
#include <string>
#include <cstdlib>

#include "MySet.h"

#define EMPTY "EMPTY"
#define SEPERATOR ","

/**
 * Constructor
 */
MySet::MySet() {
    init();
};

/**
 * Copy constructor. Adds all data from other with add method
 */
MySet::MySet(const MySet& other) {
    init();
    MySet::Node* pNode = other.getTail();
    while(pNode)
    {
        add(pNode->getKey(), pNode->getData());
        pNode = pNode->getPrev();
    }
};

/**
 * initalizing for empty set
 */
void MySet::init() {
    pHead = nullptr;
    pTail = nullptr;
    mySetSize = 0;
}

/**
 * Destructor. Handls deletion of all related nodes.
 */
MySet::~MySet() {
    MySet::Node* pNode = pHead;
    MySet::Node* temp;
    while(pNode)
    {
        temp = pNode;
        pNode = pNode->getNext();
        delete temp;
    }
};

/**
 * Assignment operator overload.
 * I have used the copy-and-swap idiom here. It means that the asignment operator is using
 * the copy constructor and swap method. first, a new MySet object is created using copy
 * constructor. than, a new empty MySet is created and it's values are swap with the previous one.
 * lastly, the empty MySet is deleted and the new full one returned.
 * I chose to use this signature and not the maybe more intuitive:
 *  MySet& operator=(const MySet& other);
 *  otherwise i would have to copy the other object inside the
 *  method itself which is usually slower than let the compiler do it.
 */
MySet& MySet::operator=(MySet other) {
    swap(*this, other);
    return *this;
}

/**
 * swap method. uses std:swap for each member
 */
void MySet::swap(MySet& first, MySet& second) {
    using std::swap;
    swap(first.pHead, second.pHead);
    swap(first.pTail, second.pTail);
    swap(first.mySetSize, second.mySetSize);
}

/**
 * Node constructor
 */
MySet::Node::Node(const std::string &key, const double data) {
    this->key = key;
    this->data = data;
    this->pNext = nullptr;
    this->pPrev = nullptr;
}

/**
 * data setter
 */
void MySet::Node::setData(double data) {
    this->data = data;
}

/**
 * add (key,data) to set. uses findNode() to check if key exists already
 */
void MySet::add(const std::string &key, double data) {
    if (!this->pHead)
    {
        this->pHead = new MySet::Node(key, data);
        this->pTail = this->pHead;
        this->mySetSize++;
    }
    else
    {
        // Look for a node with the same key and update it's data
        MySet::Node* pNode = findNode(key);
        if (pNode)
        {
            pNode->setData(data);
        }
        else
        {
            // If no node with same key exists, add new one
            MySet::Node* newNode = new MySet::Node(key, data);
            this->pHead->setPrev(newNode);
            newNode->setNext(this->pHead);
            this->pHead = newNode;
            this->mySetSize++;
        }
    }
};


/**
 * prints the set.
 */
void MySet::printSet() const {
    MySet::Node* pNode = this->pHead;
    if (pNode)
    {
        while(pNode)
        {
            std::cout << pNode->getKey() << SEPERATOR << pNode->getData() << std::endl;
            pNode = pNode->getNext();
        }
    }
    else
    {
        std::cout << EMPTY << std::endl;
    }
};

/**
 * removing key from set.
 * redirecting pointers of next and prev and handeling memory clean up.
 */
int MySet::remove(const std::string &key) {
    MySet::Node* pNode = findNode(key);
    if (pNode)
    {
        if (pNode == this->pHead)
        {
            this->pHead = pNode->getNext();
        }
        else
        {
            pNode->getPrev()->setNext(pNode->getNext());
        }
        if (pNode == this->pTail)
        {
            this->pTail = pNode->getPrev();
        }
        else
        {
            pNode->getNext()->setPrev(pNode->getPrev());
        }
        
        delete pNode;
        this->mySetSize--;
        return 1;
    }
    else
    {
        return 0;
    }
};

/**
 * returns number of nodes in the set
 */
std::size_t MySet::size() const {
    return this->mySetSize;
};

/**
 * Returns a pointer to node with certain key, of nullptr if key not exists in the set.
 */
MySet::Node* MySet::findNode(const std::string &key) {
    MySet::Node* pNode = this->pHead;
    while(pNode)
    {
        if (pNode->getKey() == key)
        {
            return pNode;
        }
        pNode = pNode->getNext();
    }
    return nullptr;
};

/**
 * const version of previuos method.
 * Implementation is same but i've decided to keep it that way, as the preffered solution from the
 * book: Effective C++, 3d ed by Scott Meyers, p. 23, is not readable  and too complicated for only
 * 5 lines of code.
 *
 * @return a constant pointer to node with certain key, of nullptr if key not exists in the set.
 */
const MySet::Node* MySet::findNode(const std::string &key) const {
    MySet::Node* pNode = this->pHead;
    while(pNode)
    {
        if (pNode->getKey() == key)
        {
            return pNode;
        }
        pNode = pNode->getNext();
    }
    return nullptr;
};

 
/**
 * Iterating over the set and sums it's nodes data.
 */
double MySet::sumSet() const {
    double result = 0;
    MySet::Node* pNode = this->pHead;
    while(pNode)
    {
        result += pNode->getData();
        pNode = pNode->getNext();
    }
    return result;
};

/**
 * Iterating over the set and sums it's nodes data.
 */
double MySet::totWeight() const {
    double result = 0;
    MySet::Node* pNode = this->pHead;
    while(pNode)
    {
        result += myHashFunction(pNode->getKey());
        pNode = pNode->getNext();
    }
    return result;
};

/**
 * Hash function as discribed in .h file.
 * Implemented using casting chars to int.
 */
int MySet::myHashFunction(const std::string &str) {
    int result = 0;
    for (size_t i=0; i < str.length(); i++)
    {
        result += str[i];
    }
    return result;
};

/**
 * loop threw all nodes in input set and return true iff the input key exists.
 * In addition, if the key was found, the data reference of is updated.
 */
bool MySet::isInSet(const std::string &key, double& data) const {
    const MySet::Node* pNode = findNode(key);
    if(pNode)
    {
        data = pNode->getData();
        return true;
    }
    return false;
};

/**
 * loop threw all nodes in input set and return true iff the key parametr exists
 */
bool MySet::isInSet(const std::string &key) const {
    MySet::Node* pNode = this->pHead;
    while(pNode)
    {
        if (pNode->getKey() == key)
        {
            return true;
        }
        pNode = pNode->getNext();
    }
    return false;
};

/**
 * Delegation to double > operator
 */
bool MySet::operator>(const MySet& other) const {
    return this->totWeight() > other.totWeight();
};

/**
 * Delegation to double < operator
 */
bool MySet::operator<(const MySet& other) const {
    return this->totWeight() < other.totWeight();
};

/**
 * Delegation to double == operator
 */
bool MySet::operator==(const MySet& other) const {
    return this->totWeight() == other.totWeight();
};

/**
 * Helper function for the &, - operators.
 * It loops threw all of the left set and and only nodes that are in other set when b is true
 * or only nodes that aren't in the other set if b is false.
 */
MySet oHelper(const MySet& other, MySet::Node::Node* pHead, bool b) {
    MySet result;

    MySet::Node* pNode = pHead;
    while (pNode)
    {
        if (other.isInSet(pNode->getKey()) == b)
        {
            result.add(pNode->getKey(), pNode->getData());
        }
        pNode = pNode->getNext();
    }
    return result;
};

/**
 * Using oHelper with b = false.
 */
MySet MySet::operator-(const MySet& other) const {
    return oHelper(other, this->getHead(), false);
};

/**
 * Using oHelper with b = true.
 */
MySet MySet::operator&(const MySet& other) const {
    return oHelper(other, this->getHead(), true);
};

/**
 *
 * The operator initializing result with other using copy consructor. than adds all items
 * of the left side, so if a key already exists, the data will change to the data of the node from
 * left side.
 *
 */
MySet MySet::operator|(const MySet& other) const {
    MySet result = other;
    
    // Add all nodes from left side
    MySet::Node* pNode = this->pHead;
    while (pNode)
    {
        result.add(pNode->getKey(), pNode->getData());
        pNode = pNode->getNext();
    }
    return result;
};
