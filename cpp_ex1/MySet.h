/**
 * MySet.h
 *
 * --------------------------------------------------------------------------------------
 * General: This class represents a Set, a data structure of distinct objects 
 *
 *  Methods: 
 *   MySet() - Constructor
 *   MySet(other) - Copy constructor
 *  ~MySet()         - Destructor
 *  
 *  add                  - Add a string to the MySet. and add the element to
 *                         the begin of of the data structure.
 *                         If the element already exists , change its data to the
 *                         input parameter. 
 * 								parameters: string(key),double(data)
 *              	         return type void
 *
 *  remove               - Remove a string from the MySet. 
 *                         Return the numbr of removed elements (0/1)
 * 								parameters: key
 *              	         return type size_t 
 *
 *  isInSet            - Return true if the element is in the Set, or false otherwise.
 *                         If the element exists in the Set, return in 'data' its appropriate data
 *                         value. Otherwise don't change the value of 'data'.
 * 								parameters: key,data
 *									return type: bool
 *
 *  size                 - Return number of elements stored in the Set.
 *              	         return type size_t 
 *
 *  sumSet              - Return sum of all data elements stored in the Set.
 *              	         return type double
 *
 *
 *  printSet             - print Set contents.
 *
 *
 *  totWeight            - Return the total myHashFunction weight of all the Set keys
 *              	         return type double
 *
 *  operator>            - check if totWeight(this) > totWeight(other)
 *
 *  operator<            - check if totWeight(this) < totWeight(other)
 *
 *  operator==           - check if totWeight(this) == totWeight(other)
 *
 *  operator&            - return new set with nodes of the intersection between left and right
 *
 *  operator-            - return new set with nodes: Left\Right
 *
 *  operator|            - return new set with both nodes of left and right
 *
 *  operator=            - assignment operator
 *
 * --------------------------------------------------------------------------------------
 */
#ifndef MY_SET_H
#define MY_SET_H


/**
 * The definition of the Set
 */
class MySet
{
    
public:
    /**
     * Nested Class - Node
     * Holds key, data and two pointers - next and prev.
     */
    class Node
    {
    public:
        /**
         * Constructor
         * @param key key for new node
         * @param data data for new node
         */
        Node(const std::string &key, const double data);
        
        // Getters
        
        /**
         * Getter for key member
         * @return key
         */
        std::string getKey() const { return this->key; };
        
        /**
         * Getter for data member
         * @return data
         */
        double getData() const { return this->data; };
        
        /**
         * Getter for pNext member
         * @return next pointer
         */
        Node* getNext() { return this->pNext; };
        
        /**
         * Getter for pPrev member
         * @return prev pointer
         */
        Node* getPrev() { return this->pPrev; };
        
        // Setters
        
        /**
         * Setter for key member
         * @param string for key
         */
        void setKey(const std::string &key);
        
        /**
         * Setter for data member
         * @param data double
         */
        void setData(double data);
        
        /**
         * Setter for pNext member
         * @param node pointer
         */
        void setNext(Node* pNext) { this->pNext = pNext; };
        
        /**
         * Setter for pPrev member
         * @param node pointer
         */
        void setPrev(Node* pPrev) { this->pPrev = pPrev; };

    private:
        std::string key; /**< the key of this node */
        double data; /**< the data of this node */
        Node* pNext; /**< pointer to next node in set */
        Node* pPrev; /**< pointer to prev node in set */
    };
    

    MySet();
    MySet(const MySet& other);
    ~MySet();
    

    void add(const std::string &key, double data);
    int remove(const std::string &key);
    void printSet() const;
    bool isInSet(const std::string &key, double& data) const;
    std::size_t size() const;
    double sumSet() const;
    double totWeight() const;

    Node* getHead() const { return this->pHead; };
    Node* getTail() const { return this->pTail; };
    bool isInSet(const std::string &key) const;
    void removeAll();
    void swap(MySet& first, MySet& second);
    

    bool operator>(const MySet& other) const;
    bool operator<(const MySet& other) const;
    bool operator==(const MySet& other) const;
    MySet operator-(const MySet& other) const;
    MySet operator|(const MySet& other) const;
    MySet operator&(const MySet& other) const;
    MySet& operator=(MySet other);
    
	/**
	 * The hash function.
	 * Input parameter - any C++ string.
	 * Return value: the hash value - the sum of all the characters of the string
	 *   (according to their ASCII value). The hash value of the
	 *   empty string is 0 (since there are no characters, their sum according to
	 *   the ASCII value is 0).
	 * NOTE: In a better design the function would have belong to the string class
	 *	 (or to a class that is derived from std::string). We implement it as a "stand 
	 *	 alone" function since you didn't learn inheritance yet. Keep the function
	 *	 global (it's important to the automatic tests).
	 */
	static int myHashFunction(const std::string &str);

private:
    Node* pHead; /**< set's first node */
    Node* pTail; /**< set's last node */
    std::size_t mySetSize; /**< set's size in size_t */
    void init(); /**< initializer for empty set's members */
    
    /**
     * Find node by key
     * @param key to look for
     * @Return a constant pointer to node with certain key, of nullptr if key not exists in the set.
     */
    Node* findNode(const std::string &key);

    /**
     * const version of previuos method.
     * @param key to look for
     * @Return a constant pointer to node with certain key, of nullptr if key not exists in the set.
     */
    const Node* findNode(const std::string &key) const;
};

#endif
